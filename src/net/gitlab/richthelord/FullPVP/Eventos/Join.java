package net.gitlab.richthelord.FullPVP.Eventos;

import net.gitlab.richthelord.FullPVP.Main;
import net.gitlab.richthelord.FullPVP.Utils.FPlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class Join implements Listener{
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        FPlayerUtils.runCreatePlayerVar(e.getPlayer());
    }

}
