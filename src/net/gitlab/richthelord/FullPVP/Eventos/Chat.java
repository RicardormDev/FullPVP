package net.gitlab.richthelord.FullPVP.Eventos;

import net.gitlab.richthelord.FullPVP.Player.FPlayer;
import net.gitlab.richthelord.FullPVP.Player.FPlayerManager;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class Chat implements Listener{

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerChatEvent(AsyncPlayerChatEvent e) {
        FPlayer fPlayer = FPlayerManager.getManager().getPlayer(e.getPlayer());

        if(fPlayer != null) {
            e.setFormat("§8[§6Lv. " + fPlayer.getLevel() + "§8] §8[" + fPlayer.getTitle().getColor() + fPlayer.getTitle().getTag() + "§8]§r " + e.getFormat());
            //e.setFormat("§8(§aLV. " + fPlayer.getLevel() + "§8) §8[§b" + fPlayer.getTitle().getColor() + fPlayer.getTitle().toString() + "§8]§r " + e.getFormat());
        }
    }

}
