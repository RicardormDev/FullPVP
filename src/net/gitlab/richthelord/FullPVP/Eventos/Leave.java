package net.gitlab.richthelord.FullPVP.Eventos;

import net.gitlab.richthelord.FullPVP.Exceptions.CouldntSavePlayerException;
import net.gitlab.richthelord.FullPVP.Main;
import net.gitlab.richthelord.FullPVP.Player.FPlayer;
import net.gitlab.richthelord.FullPVP.Player.FPlayerManager;
import net.gitlab.richthelord.FullPVP.Utils.FPlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class Leave implements Listener{

    @EventHandler(ignoreCancelled = true, priority = EventPriority.NORMAL)
    public void onPlayerLeaveEvent(PlayerQuitEvent e) {
        Bukkit.getScheduler().runTaskAsynchronously(Main.getPlugin(), () -> {
            FPlayer fplayer = FPlayerManager.getManager().getPlayer(e.getPlayer());

            if(fplayer == null) {
                try {
                    throw new CouldntSavePlayerException("The variable fplayer couldn't be saved (this wasn't created when the player joined).");
                } catch (CouldntSavePlayerException e1) {
                    e1.printStackTrace();
                }
                return;
            }

            FPlayerUtils.updatePlayer(fplayer);
            FPlayerManager.getManager().removePlayer(fplayer);
        });
    }

}
