package net.gitlab.richthelord.FullPVP.Eventos;

import net.gitlab.richthelord.FullPVP.Assists.Assists;
import net.gitlab.richthelord.FullPVP.Assists.AssistsManager;
import net.gitlab.richthelord.FullPVP.Main;
import net.gitlab.richthelord.FullPVP.Player.FPlayer;
import net.gitlab.richthelord.FullPVP.Player.FPlayerManager;
import net.gitlab.richthelord.FullPVP.Utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class KillDead implements Listener{

    private final ItemStack appel = new ItemBuilder().setDisplayName("&3&lManzana del Acechador").setMaterial(Material.GOLDEN_APPLE).setQuantity(1).build();

    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void onPlayerDamageEvent(EntityDamageByEntityEvent e) {
        Entity killer = e.getDamager();
        Entity dead   = e.getEntity();

        if(!(killer instanceof Player))
            return;

        if(!(dead instanceof Player))
            return;

        Bukkit.getScheduler().runTaskAsynchronously(Main.getPlugin(), () -> {       });
            FPlayer kF = FPlayerManager.getManager().getPlayer((Player) killer);
            FPlayer dF = FPlayerManager.getManager().getPlayer((Player) dead);

            Assists a = AssistsManager.getManager().getAssistsTwo(dF, kF);

            double dG = e.getFinalDamage();
            double life = dF.getPlayer().getHealth();
            double finalDamage = life - dG;


            if(finalDamage <= 0) {
                if(kF != null) {
                    kF.updateKill();
                    kF.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 20*4, 2));
                    kF.getPlayer().getInventory().addItem(appel);
                }

                if(dF != null) {
                    dF.updateDead();
                }

                if(a != null) {

                    for(Assists aall : AssistsManager.getManager().getAllFrom(dF)){

                        if(aall.getHitter() != kF){
                            aall.updateAssist();
                            aall.getHitter().getPlayer().sendMessage(c("&8&l[&3&lVolk&c&lPvP&8&l]&a " + dF.getName() + " a muerto y recibiste asistencia."));
                        }

                        aall.end();
                    }

                }

            }


            if(a != null) {
                a.setI(8);
            } else {
                AssistsManager.getManager().registerAssistance(new Assists(dF, kF));
            }
    }

    private String c (String msg) {
        return ChatColor.translateAlternateColorCodes('&', msg);
    }
}
