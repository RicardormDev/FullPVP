package net.gitlab.richthelord.FullPVP.Eventos;

import net.gitlab.richthelord.FullPVP.Player.FPlayer;
import net.gitlab.richthelord.FullPVP.Player.FPlayerManager;
import net.gitlab.richthelord.FullPVP.Utils.ItemBuilder;
import net.gitlab.richthelord.FullPVP.Utils.LocationUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class Combat implements Listener{

    private final List<Location> doorToKil = LocationUtils.getLocations(
            new Location(Bukkit.getWorld("Spawn"), -192D, 87D, -66D),
            new Location(Bukkit.getWorld("Spawn"), -192D, 96, -56D)
    );

    private final ItemStack[] armor = new ItemStack[] {
            new ItemBuilder().setDisplayName("&6&lBotas del Guerrero").setMaterial(Material.IRON_BOOTS).addEnchant(Enchantment.DURABILITY, 1).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build(),
            new ItemBuilder().setDisplayName("&6&lPantalon del Guerrero").setMaterial(Material.IRON_LEGGINGS).addEnchant(Enchantment.DURABILITY, 1).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build(),
            new ItemBuilder().setDisplayName("&6&lPeto del Guerrero").setMaterial(Material.IRON_CHESTPLATE).addEnchant(Enchantment.DURABILITY, 1).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build(),
            new ItemBuilder().setDisplayName("&6&lCasco del Guerrero").setMaterial(Material.IRON_HELMET).addEnchant(Enchantment.DURABILITY, 1).addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1).build(),
    };
    private final ItemStack[] items = new ItemStack[] {
            new ItemBuilder().setDisplayName("&3&lEspada del Guerrero").setMaterial(Material.IRON_SWORD).addEnchant(Enchantment.DURABILITY, 1).build(),
            new ItemBuilder().setDisplayName("&3&lArco del Guerrero").setMaterial(Material.BOW).addEnchant(Enchantment.DURABILITY, 1).build(),
            new ItemBuilder().setDisplayName("&3&lFlechas de Adamantio").setMaterial(Material.ARROW).setQuantity(64).build(),
            new ItemBuilder().setDisplayName("&3&lCarne Fresca").setMaterial(Material.COOKED_BEEF).setQuantity(64).build(),
            new ItemBuilder().setDisplayName("&3&lManzana del Acechador").setMaterial(Material.GOLDEN_APPLE).setQuantity(2).build(),
            new ItemBuilder().setDisplayName("&3&lCaña del Asesino").setMaterial(Material.FISHING_ROD).addEnchant(Enchantment.DURABILITY, 1).setQuantity(1).build(),
    };


    public Combat() { }

    @EventHandler
    public void goToCombat(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        if(doorToKil.contains(new Location(player.getWorld(), player.getLocation().getBlockX(), player.getLocation().getBlockY(), player.getLocation().getBlockZ()))) {
            player.getInventory().setArmorContents(armor);
            player.getInventory().setItem(0, items[0]);
            player.getInventory().setItem(1, items[5]);
            player.getInventory().setItem(2, items[1]);
            player.getInventory().setItem(4, items[3]);
            player.getInventory().setItem(5, items[4]);
            player.getInventory().setItem(8, items[2]);
        }
    }

    @EventHandler
    public void pressGold(PlayerInteractEvent e) {
        if(e.getAction() == Action.PHYSICAL) {
            if(e.getClickedBlock().getType() == Material.GOLD_PLATE) {
                Player player = e.getPlayer();
                player.getInventory().setArmorContents(armor);
                player.getInventory().setItem(0, items[0]);
                player.getInventory().setItem(1, items[5]);
                player.getInventory().setItem(2, items[1]);
                player.getInventory().setItem(4, items[3]);
                player.getInventory().setItem(5, items[4]);
                player.getInventory().setItem(8, items[2]);
            }
        }
    }

    @EventHandler
    public void onDeathEvent(PlayerDeathEvent e) {
        e.getDrops().clear();
        e.getEntity().spigot().respawn();

    }

}
