package net.gitlab.richthelord.FullPVP;

import net.gitlab.richthelord.FullPVP.Assists.Assists;
import net.gitlab.richthelord.FullPVP.Assists.AssistsManager;
import net.gitlab.richthelord.FullPVP.Player.FPlayer;
import net.gitlab.richthelord.FullPVP.Player.FPlayerManager;
import net.gitlab.richthelord.FullPVP.Player.PlayerUtils;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commands implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if(cmd.getName().equalsIgnoreCase("pvp")) {

            if(args.length == 0) {

                sender.sendMessage(ChatColor.GOLD + "/pvp admin ");
                sender.sendMessage(ChatColor.GOLD + "/pvp menu ");

            } else if(args[0].equalsIgnoreCase("menu")) {
                if(!(sender instanceof Player)) {
                    sender.sendMessage(ChatColor.RED + "Este comando es solo para jugadores.");
                    return true;
                }

            } else if(args[0].equalsIgnoreCase("admin")) {
                if(sender.hasPermission("FullPVP.admin") || sender.isOp()) {

                    if(args.length < 2) {
                        sender.sendMessage(PlayerUtils.ErrorCMD);
                        return true;
                    }

                    if(args[1].equalsIgnoreCase("add")) {



                        if(args.length < 5) {
                            sender.sendMessage("§cError de Comando. /pvp admin help");
                            return true;
                        }

                        Player target = Bukkit.getServer().getPlayer(args[4]);

                        if(target == null) {
                            sender.sendMessage("§cJugador Offline.");
                            return true;
                        }



                        if(args[2].equalsIgnoreCase("level")) {
                            sender.sendMessage(PlayerUtils.addTo(PlayerUtils.Getters.LEVEL, args));
                        } else if(args[2].equalsIgnoreCase("kills")) {
                            sender.sendMessage(PlayerUtils.addTo(PlayerUtils.Getters.KILLS, args));
                        } else if(args[2].equalsIgnoreCase("xp")) {
                            sender.sendMessage(PlayerUtils.addTo(PlayerUtils.Getters.XP, args));
                        } else {
                            sender.sendMessage("§cError de Comando. /pvp admin help");
                        }



                    } else if(args[1].equalsIgnoreCase("remove")) {
                        if(args[2].equalsIgnoreCase("level")) {
                           sender.sendMessage(PlayerUtils.removeTo(PlayerUtils.Getters.LEVEL, args));
                        } else if(args[2].equalsIgnoreCase("kills")) {
                            sender.sendMessage(PlayerUtils.removeTo(PlayerUtils.Getters.KILLS, args));
                        } else if(args[2].equalsIgnoreCase("xp")) {
                            sender.sendMessage(PlayerUtils.removeTo(PlayerUtils.Getters.XP, args));
                        } else {
                            sender.sendMessage("§cError de Comando. /pvp admin help");
                        }
                    } else if(args[1].equalsIgnoreCase("set")) {
                        if(args[2].equalsIgnoreCase("level")) {
                            sender.sendMessage(PlayerUtils.setTo(PlayerUtils.Getters.LEVEL, args));
                        } else if(args[2].equalsIgnoreCase("kills")) {
                            sender.sendMessage(PlayerUtils.setTo(PlayerUtils.Getters.KILLS, args));
                        } else if(args[2].equalsIgnoreCase("xp")) {
                            sender.sendMessage(PlayerUtils.setTo(PlayerUtils.Getters.XP, args));
                        } else {
                            sender.sendMessage("§cError de Comando. /pvp admin help");
                        }
                    } else if(args[1].equalsIgnoreCase("get")) {

                        if(args[2].equalsIgnoreCase("level")) {
                            sender.sendMessage(PlayerUtils.getOf(PlayerUtils.Getters.LEVEL, args));
                        } else if(args[2].equalsIgnoreCase("kills")) {
                            sender.sendMessage(PlayerUtils.getOf(PlayerUtils.Getters.KILLS, args));
                        } else if(args[2].equalsIgnoreCase("xp")) {
                            sender.sendMessage(PlayerUtils.getOf(PlayerUtils.Getters.XP, args));
                        } else {
                            sender.sendMessage("§cError de Comando. /pvp admin help");
                        }

                    } else if(args[1].equalsIgnoreCase("help")) {
                        sender.sendMessage(ChatColor.GOLD + "/pvp admin  add [level | kills | xp ] [number] [player]");
                        sender.sendMessage(ChatColor.GOLD + "/pvp admin  set [level | kills | xp ] [number] [player]");
                        sender.sendMessage(ChatColor.GOLD + "/pvp admin  remove [level | kills | xp ] [number] [player]");
                        sender.sendMessage(ChatColor.GOLD + "/pvp admin  get [level | kills | xp ] [player]");
                    } else if(args[1].equalsIgnoreCase("fixLVLFromKills")) {

                        Main.getPlugin().getSqlBuilder().fixXPLEVEL();
                        sender.sendMessage("FIXING LEVEL AND XP");

                    }  else if(args[1].equalsIgnoreCase("test")) {
                        //FPlayerManager.getManager().getPlayer((Player)sender).updateLevel();
                        //sender.sendMessage("UPDATED to " + FPlayerManager.getManager().getPlayer((Player)sender).getTitle().toString());

                        for(Assists a : AssistsManager.getManager().getAll()){
                            sender.sendMessage(a.getI() + " " + a.getHitter().getName() + " " + a.getTarget().getName() + " " + a.toString());
                        }
                    }

                    else {
                        sender.sendMessage(FPlayerManager.getManager().getFirst().getTitle().toString());
                        sender.sendMessage("§cError de Comando. /pvp admin help");
                    }
                }
            }
        }

        return true;
    }
}
