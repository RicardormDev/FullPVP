package net.gitlab.richthelord.FullPVP;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import be.maximvdw.placeholderapi.PlaceholderReplaceEvent;
import be.maximvdw.placeholderapi.PlaceholderReplacer;
import net.gitlab.richthelord.FullPVP.Eventos.*;
import net.gitlab.richthelord.FullPVP.Exceptions.CouldntSavePlayerException;
import net.gitlab.richthelord.FullPVP.Player.FPlayer;
import net.gitlab.richthelord.FullPVP.Player.FPlayerManager;
import net.gitlab.richthelord.FullPVP.Tops.*;
import net.gitlab.richthelord.FullPVP.Utils.FPlayerUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;

public class Main extends JavaPlugin{

    private static Plugin leaderheads;
    private SQLBuilder sqlBuilder;
    private static Main main;
    public boolean PlaceHoldersActive;

    @Override
    public void onEnable() {
        sqlBuilder = new SQLBuilder();
        main = this;

        try {
            sqlBuilder.openConnection();
            sqlBuilder.startSQL();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        leaderheads = Bukkit.getPluginManager().getPlugin("LeaderHeads");

        getCommand("pvp").setExecutor(new Commands());
        Bukkit.getServer().getPluginManager().registerEvents(new Join(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new Chat(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new KillDead(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new Leave(), this);
        Bukkit.getServer().getPluginManager().registerEvents(new Combat(), this);

        for(Player p : Bukkit.getOnlinePlayers()) {
            FPlayerUtils.runCreatePlayerVar(p);
        }

        if(leaderheads != null) {
            new TopKills();
            new TopLevel();
            new TopKDA();
            new TopDeads();
            new TopAssists();
        }

        if(Bukkit.getPluginManager().isPluginEnabled("MVdWPlaceholderAPI")) {
            this.PlaceHoldersActive = true;

            PlaceholderAPI.registerPlaceholder(this, "FFAKills", (e) -> {
                    if(e.isOnline()) {
                        return "" + FPlayerManager.getManager()
                                .getPlayer(e.getPlayer())
                                .getKills();
                    }
                    return "nope";
            });

            PlaceholderAPI.registerPlaceholder(this, "FFALevel", (e) -> {
                if(e.isOnline()) {
                    return "" + FPlayerManager.getManager().getPlayer(e.getPlayer().getUniqueId()).getLevel();
                }
                return "nope";
            });

            PlaceholderAPI.registerPlaceholder(this, "FFAKDA", (e) -> {
                if(e.isOnline()) {
                    return "" + FPlayerManager.getManager().getPlayer(e.getPlayer().getUniqueId()).getKda();
                }
                return "nope";
            });

            PlaceholderAPI.registerPlaceholder(this, "FFADeads", (e) -> {
                if(e.isOnline()) {
                    return "" + FPlayerManager.getManager().getPlayer(e.getPlayer().getUniqueId()).getDeads();
                }
                return "nope";
            });

            PlaceholderAPI.registerPlaceholder(this, "FFAAssists", (e) -> {
                if(e.isOnline()) {
                    return "" + FPlayerManager.getManager().getPlayer(e.getPlayer().getUniqueId()).getAssistances();
                }
                return "nope";
            });

        }
    }

    @Override
    public void onDisable() {

        for(Player player : Bukkit.getOnlinePlayers()) {
            FPlayer fplayer = FPlayerManager.getManager().getPlayer(player);

            if(fplayer == null) {
                try {
                    throw new CouldntSavePlayerException("The variable fplayer couldn't be saved (this wasn't created when the player joined).");
                } catch (CouldntSavePlayerException e1) {
                    e1.printStackTrace();
                }
                return;
            }

            FPlayerUtils.updatePlayer(fplayer);
        }

        sqlBuilder.closeConnection();
    }

    public Plugin getLeaderheads() {
        return leaderheads;
    }

    public SQLBuilder getSqlBuilder() {
        return sqlBuilder;
    }

    public static Main getPlugin() {
        return main;
    }
}


