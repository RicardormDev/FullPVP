package net.gitlab.richthelord.FullPVP.Assists;

import net.gitlab.richthelord.FullPVP.Player.FPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

public class Assists extends BukkitRunnable{

    private FPlayer target;
    private FPlayer hitter;
    private int i;


    public Assists(FPlayer target, FPlayer hitter) {
        this.target = target;
        this.hitter = hitter;
        this.i = 8;
    }

    public int getI() {
        return i;
    }

    public FPlayer getHitter() {
        return hitter;
    }

    public FPlayer getTarget() {
        return target;
    }

    public void setI(int i) {
        this.i = i;
    }

    public void end() {
        AssistsManager.getManager().unregisterAssistance(this);
        cancel();
    }

    public void updateAssist() {
        hitter.updateAssistance();
    }

    @Override
    public void run() {

        if(i <= 0) {
            end();
            cancel();
        }

        i--;
    }


}
