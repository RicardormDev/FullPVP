package net.gitlab.richthelord.FullPVP.Assists;

import net.gitlab.richthelord.FullPVP.Main;
import net.gitlab.richthelord.FullPVP.Player.FPlayer;

import java.util.ArrayList;
import java.util.List;

public class AssistsManager {

    private static AssistsManager manager = new AssistsManager();
    public static AssistsManager getManager() {
        return manager;
    }

    private ArrayList<Assists> assists;
    public AssistsManager() {
        assists = new ArrayList<>();
    }

    public void registerAssistance(FPlayer hitter, FPlayer target) {
        Assists a = new Assists(target, hitter);
        assists.add(a);
        a.runTaskTimerAsynchronously(Main.getPlugin(), 0, 20);
    }


    public void registerAssistance(Assists a) {
        assists.add(a);
        a.runTaskTimerAsynchronously(Main.getPlugin(), 0, 20);
    }

    public void unregisterAssistance(FPlayer target) {
        assists.remove(getAssists(target));
    }

    public void unregisterAssistance(Assists a) {
        assists.remove(a);
    }

    public Assists getAssists(FPlayer target) {
        for(Assists a : assists) {
            if(a.getTarget() == target) {
                return a;
            }
        }
        return null;
    }

    public Assists getAssistsTwo(FPlayer target, FPlayer hitter) {
        for(Assists a : assists) {
            if(a.getTarget() == target && a.getHitter() == hitter) {
                return a;
            }
        }
        return null;
    }

    public List<Assists> getAllFrom(FPlayer target) {
        List<Assists> ss = new ArrayList<>();
        for(Assists a : assists) {
            if(a.getTarget() == target) {
                ss.add(a);
            }
        }

        return ss;
    }

    public Assists getAssistsH(FPlayer hitter) {
        for(Assists a : assists) {
            if(a.getHitter() == hitter) {
                return a;
            }
        }
        return null;
    }

    public List<Assists> getAll() {
        return assists;
    }
}
