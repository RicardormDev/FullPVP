package net.gitlab.richthelord.FullPVP.Tops;

import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import net.gitlab.richthelord.FullPVP.Player.FPlayerManager;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class TopAssists extends OnlineDataCollector{
    public TopAssists() {
        super("TAssists", "FullPVP", BoardType.DEFAULT, "&3&lTOP ASISTENCIAS", "tassists", Arrays.asList(null, null, "&3{amount}", "&3asistencias"));
    }

    @Override
    public Double getScore(Player player) {
        return (double)  FPlayerManager.getManager().getPlayer(player).getAssistances();
    }
}
