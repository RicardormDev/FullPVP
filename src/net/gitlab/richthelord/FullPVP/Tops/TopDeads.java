package net.gitlab.richthelord.FullPVP.Tops;

import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import net.gitlab.richthelord.FullPVP.Player.FPlayerManager;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class TopDeads extends OnlineDataCollector{
    public TopDeads() {
        super("TDeads", "FullPVP", BoardType.DEFAULT, "&3&lTOP MUERTES", "tdeads", Arrays.asList(null, null, "&3{amount} Deads", null));
    }

    @Override
    public Double getScore(Player player) {
        return (double) FPlayerManager.getManager().getPlayer(player).getDeads();
    }
}
