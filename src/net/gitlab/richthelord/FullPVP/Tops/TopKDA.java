package net.gitlab.richthelord.FullPVP.Tops;

import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import net.gitlab.richthelord.FullPVP.Player.FPlayerManager;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class TopKDA extends OnlineDataCollector{

    public TopKDA() {
        super("TKDA", "FullPVP", BoardType.DEFAULT, "&3&lTOP KDA", "tkda", Arrays.asList(null, null, "&3{amount} KDA", null));
    }

    @Override
    public Double getScore(Player player) {
        return FPlayerManager.getManager().getPlayer(player).getKda();
    }

}
