package net.gitlab.richthelord.FullPVP.Tops;

import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import net.gitlab.richthelord.FullPVP.Player.FPlayerManager;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class TopLevel extends OnlineDataCollector{

    public TopLevel() {
        super("TopLevel", "FullPVP", BoardType.DEFAULT, "&3&lTOP LEVELS", "TLevel", Arrays.asList(null, null, "&3{amount} Level", null));
    }

    @Override
    public Double getScore(Player player) {
        return (double)  FPlayerManager.getManager().getPlayer(player).getLevel();
    }
}
