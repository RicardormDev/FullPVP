package net.gitlab.richthelord.FullPVP.Tops;

import me.robin.leaderheads.datacollectors.OnlineDataCollector;
import me.robin.leaderheads.objects.BoardType;
import net.gitlab.richthelord.FullPVP.Player.FPlayerManager;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class TopKills extends OnlineDataCollector{

    public TopKills() {
        super("TKills", "FullPVP", BoardType.DEFAULT, "&3&lTOP KILLS", "tkills", Arrays.asList(null, null, "&3{amount} Kills", null));
    }

    @Override
    public Double getScore(Player player) {
        return (double)  FPlayerManager.getManager().getPlayer(player).getKills();
    }

}
