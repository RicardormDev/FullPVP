package net.gitlab.richthelord.FullPVP.Exceptions;

public class CouldntSavePlayerException extends Exception{


    public CouldntSavePlayerException(){
        super();
    }

    public CouldntSavePlayerException(String message) {
        super(message);
    }

    public CouldntSavePlayerException(String message, Throwable cause) {
        super(message, cause);
    }

    public CouldntSavePlayerException(Throwable throwable) {
        super(throwable);
    }

}
