package net.gitlab.richthelord.FullPVP;

import net.gitlab.richthelord.FullPVP.Player.FPlayer;

import java.sql.*;
import java.util.List;
import java.util.UUID;

public class SQLBuilder {

    private final static String username = "VolkMC";
    private final static String password = ")84*BLuoG^%Y<[WnLNeY!<:Q9TByu1";

    private Connection connection;
    private Statement statement;


    public void openConnection() throws SQLException, ClassNotFoundException {
        if(connection != null && !connection.isClosed()) {
            return;
        }

        synchronized (this) {
            if (connection != null && !connection.isClosed()) {
                return;
            }

            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/VolkData", username, password);
        }
    }

    public void closeConnection() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void startSQL() {
        try {
            statement = connection.createStatement();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Statement getStatement() {
        return statement;
    }

    public int getPlayerID(String name) {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT ID FROM PlayerData WHERE name= '" + name + "';");
            while(resultSet.next()) {
                return resultSet.getInt("ID");
            }
            resultSet.close();
        } catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

    public int getPlayerID(UUID uniqueId) {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT ID FROM PlayerData WHERE UniqueId= '" + uniqueId.toString() + "';");
            while(resultSet.next()) {
                return resultSet.getInt("ID");
            }
            resultSet.close();
        } catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

    public int getPlayerIDFromFullPVPTable(int ID) {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT PlayerID FROM FullPVP WHERE PlayerID= " + ID + ";");
            while(resultSet.next()) {
                return resultSet.getInt("PlayerID");
            }
            resultSet.close();
        } catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        return 0;
    }


    public int getPlayerKills(int ID) {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT Kills FROM FullPVP WHERE PlayerID= " + ID + ";");
            while(resultSet.next()) {
                return resultSet.getInt("Kills");
            }
            resultSet.close();
        } catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

    public void fixXPLEVEL() {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT * FROM FullPVP");
            while(resultSet.next()) {
                int kills = resultSet.getInt("Kills");
                long XP = kills * 40;
                int level = (int) (XP / 200);

                PreparedStatement statement = connection.prepareStatement("UPDATE FullPVP SET LVL=?, XP=? WHERE PlayerID =?;");
                statement.setInt(1, level);
                statement.setLong(2, XP);
                statement.setInt(3, resultSet.getInt("PlayerID"));
                statement.executeUpdate();

            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public int getPlayerLevel(int ID) {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT LVL FROM FullPVP WHERE PlayerID= " + ID + ";");
            while(resultSet.next()) {
                return resultSet.getInt("LVL");
            }
            resultSet.close();
        } catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

    public int getPlayerXP(int ID) {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT XP FROM FullPVP WHERE PlayerID= " + ID + ";");
            while(resultSet.next()) {
                return resultSet.getInt("XP");
            }
            resultSet.close();
        } catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

    public int getDeads(int ID) {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT Deads FROM FullPVP WHERE PlayerID= " + ID + ";");
            while(resultSet.next()) {
                return resultSet.getInt("Deads");
            }
            resultSet.close();
        } catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

    public int getPlayerAssists(int ID) {
        try {
            ResultSet resultSet = statement.executeQuery("SELECT Assists FROM FullPVP WHERE PlayerID= " + ID + ";");
            while(resultSet.next()) {
                return resultSet.getInt("Assists");
            }
            resultSet.close();
        } catch (Exception e){
            e.printStackTrace();
            return 0;
        }
        return 0;
    }

    public void savePlayer(FPlayer player) {
        try {
            PreparedStatement statement = connection.prepareStatement("UPDATE FullPVP SET Kills=?, LVL=?, XP=?, Deads=?, Assists=? WHERE PlayerID = " + player.getID() + ";");
            statement.setInt(1, player.getKills());
            statement.setInt(2, player.getLevel());
            statement.setLong(3, player.getXP());
            statement.setInt(4, player.getDeads());
            statement.setInt(5, player.getAssistances());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void createPlayerOnDB(FPlayer player) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO FullPVP VALUES (?, ?, ?, ?, ?, ?);");
            statement.setInt(1, player.getID());
            statement.setInt(2, player.getKills());
            statement.setInt(3, player.getAssistances());
            statement.setInt(4, player.getLevel());
            statement.setLong(5, player.getXP());
            statement.setInt(6, player.getDeads());
            statement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
