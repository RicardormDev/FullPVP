package net.gitlab.richthelord.FullPVP.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

import java.util.ArrayList;
import java.util.List;

public class LocationUtils {

    /**
     *
     * Usa este metodo para obtener todos los puntos dentro
     * de un cuadrado basandose en 2 puntos de diferencia.
     *
     * @param loc1 Punto número 1 para obtener todos los bloques dentro.
     * @param loc2 Punto número 2 para obtener tos los bloques dentro.
     * @return Regresa una lista con todos los bloques.
     * @see Location
     */
    public static List<Location> getLocations(Location loc1, Location loc2) {
        List<Location> locs = new ArrayList<>();

        for (Double x = loc1.getX(); x <= loc2.getX(); x++) {
            for (Double y = loc1.getY(); y <= loc2.getY(); y++) {
                for (Double z = loc1.getZ(); z <= loc2.getZ(); z++) {
                    Location fLoc = new Location(loc1.getWorld(), x.intValue() , y.intValue() , z.intValue());
                    locs.add(fLoc);
                }
            }
        }

        return locs;
    }

    /**
     *
     * Saca una Location apartir de una <i>String</i>
     *
     * @param locStr La <i>String</i> con datos para hacer una Location
     * @return Location generada.
     *
     */
    public static Location deserilize(String locStr) {
        String[] a = locStr.split(":");
        return new Location(
                Bukkit.getWorld(a[0]),    // WORLD
                Double.parseDouble(a[1]), // X
                Double.parseDouble(a[2]), // Y
                Double.parseDouble(a[3]), // Z
                Float.parseFloat(a[4]),   // YAW
                Float.parseFloat(a[5])    // PITCH
        );
    }

    /**
     *
     * Obtener en una String los datos de una Location
     *
     * @param location Objeto Location a convertir
     * @return String con los datos de una Location para fácil guardado.
     *
     */
    public static String serilizeLocation(Location location) {
        return location.getWorld().getName()
                + ":" + location.getX()
                + ":" + location.getY()
                + ":" + location.getZ()
                + ":" + location.getYaw()
                + ":" + location.getPitch();
    }

}
