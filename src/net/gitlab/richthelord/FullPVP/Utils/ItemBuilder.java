package net.gitlab.richthelord.FullPVP.Utils;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * API made to create ItemStack on spigot.
 *
 * @author Ricardo Rodríguez Medina (RicardormDev)
 * @version 1.0
 *
 */
public class ItemBuilder {

    private Material material;
    private int quantity;
    private Byte data;

    private String displayName;
    private List<String> lore;
    private List<Enchant> enchants;

    /**
     *
     * Create a new object ItemBuilder
     *
     */
    public ItemBuilder() {
        enchants = new ArrayList<>();
        lore     = new ArrayList<>();

        this.quantity = 1;
        this.data = 0;
    }

    /**
     *
     * Set the material of the items stack (cannot be null)
     *
     * @param material The type that you need (It's enum)
     * @return The builder.
     * @see Material
     *
     */
    public ItemBuilder setMaterial(Material material) {
        this.material = material;
        return this;
    }

    /**
     *
     * How many you need?
     *
     * @param q The amount of items on the ItemStack
     * @return The builder.
     */
    public ItemBuilder setQuantity(int q) {
        this.quantity = q;
        return this;
    }

    /**
     *
     * If you need data (example: the orange color of the wool, 35:1
     *
     * @param d The amount.
     * @return The builder.
     */
    public ItemBuilder setData(int d) {
        this.data = (byte) d;
        return this;
    }

    /**
     *
     * The name that the player will see when he takes the item.
     *
     * @param name The name / text you need
     * @return The builder.
     */
    public ItemBuilder setDisplayName(String name) {
        this.displayName = name;
        return this;
    }

    /**
     *
     * Set the lore, the text below the name.
     * <p>This is displayed when you hover the item.</p>
     *
     * @param index The line of the text you need to be displayed.
     * @param text The text.
     * @return The builder.
     */
    public ItemBuilder setLore(int index, String text) {
        this.lore.set(index, text.replace("&", "§"));
        return this;
    }

    /**
     *
     * If you need to add an enchantment, do it here.
     *
     * @param e The type of Enchant.
     * @param level Level of the enchant.
     * @return The builder.
     */
    public ItemBuilder addEnchant(Enchantment e, int level) {
        this.enchants.add(new Enchant(e, level));
        return this;
    }

    /**
     *
     * Finally build the item into an itemstack.
     *
     * @return ItemStack ready to use.
     * @see ItemStack
     *
     */
    public ItemStack build() {
        ItemStack item = new ItemStack(material, quantity, (short) 1, data);
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(displayName.replace("&", "§"));

        if(!lore.isEmpty())
            meta.setLore(lore);

        if(!enchants.isEmpty())
            for(Enchant e : enchants) {
                meta.addEnchant(e.getE(), e.getLevel(), true);
            }

        item.setItemMeta(meta);
        return item;
    }

    /**
     *
     * The class to manage the enchant.
     */
    private class Enchant {

        private Enchantment e;
        private int level;

        /**
         *
         * The builder.
         *
         * @param e Type of Enchant.
         * @param l Level of the Enchant.
         */
        private Enchant(Enchantment e, int l) {
            this.e = e;
            this.level = l;
        }

        /**
         *
         * @return Level of the enchant.
         */
        private int getLevel() {
            return level;
        }

        /**
         *
         * @return Type of enchantment.
         */
        private Enchantment getE() {
            return e;
        }
    }
}
