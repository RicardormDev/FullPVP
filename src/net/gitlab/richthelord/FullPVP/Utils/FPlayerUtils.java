package net.gitlab.richthelord.FullPVP.Utils;

import net.gitlab.richthelord.FullPVP.Main;
import net.gitlab.richthelord.FullPVP.Player.FPlayer;
import net.gitlab.richthelord.FullPVP.Player.FPlayerManager;
import net.gitlab.richthelord.FullPVP.SQLBuilder;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class FPlayerUtils {

    private static SQLBuilder sql = Main.getPlugin().getSqlBuilder();

    public static void runCreatePlayerVar(Player player) {
        FPlayer fPlayer = FPlayerManager.getManager().getPlayer(player);

        if(fPlayer == null) {
            fPlayer = new FPlayer(player);

            int ID = sql.getPlayerID(fPlayer.getUniqueId());
            fPlayer.setID(ID);

            if(sql.getPlayerIDFromFullPVPTable(ID) == 0) {
                sql.createPlayerOnDB(fPlayer);
            } else {
                int kills = sql.getPlayerKills(ID);
                int assis = sql.getPlayerAssists(ID);
                int level = sql.getPlayerLevel(ID);
                long XP = sql.getPlayerXP(ID);
                int deads = sql.getDeads(ID);

                fPlayer.setLevel(level);
                fPlayer.setKills(kills);
                fPlayer.setAssistances(assis);
                fPlayer.setXP(XP);
                fPlayer.setDeads(deads);
            }

            FPlayerManager.getManager().addPlayer(fPlayer);
        }
    }

    public static FPlayer buildFPlayerFromSQL(int ID) {
        FPlayer fPlayer = new FPlayer();

        if(ID == 0) {
            System.out.println("USUARIO NO ENCONTRADO.");
            return null;
        }

        int kills = sql.getPlayerKills(ID);
        int assis = sql.getPlayerAssists(ID);
        int level = sql.getPlayerLevel(ID);
        long XP = sql.getPlayerXP(ID);
        int deads = sql.getDeads(ID);

        fPlayer.setID(ID);
        fPlayer.setLevel(level);
        fPlayer.setKills(kills);
        fPlayer.setAssistances(assis);
        fPlayer.setXP(XP);
        fPlayer.setDeads(deads);


        return fPlayer;
    }

    public static void updatePlayer(FPlayer player) {
        sql.savePlayer(player);
    }

    public static void createVarsForOnline() {
        for(Player player : Bukkit.getOnlinePlayers()) {
            FPlayer fPlayer = FPlayerManager.getManager().getPlayer(player);
            if(fPlayer == null) {
                runCreatePlayerVar(player);
            }
        }
    }

}
