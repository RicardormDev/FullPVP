package net.gitlab.richthelord.FullPVP.Player;

import net.gitlab.richthelord.FullPVP.Main;
import net.gitlab.richthelord.FullPVP.SQLBuilder;
import net.gitlab.richthelord.FullPVP.Utils.FPlayerUtils;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.UUID;

public class FPlayerManager {

    private static FPlayerManager manager = new FPlayerManager();
    public static FPlayerManager getManager() {
        return manager;
    }

    private ArrayList<FPlayer> players;
    private SQLBuilder sql;
    public FPlayerManager() {
        players = new ArrayList<>();
        sql = Main.getPlugin().getSqlBuilder();
    }

    public void addPlayer(FPlayer p) {
        players.add(p);
    }

    public void removePlayer(FPlayer player) {
        players.remove(player);
    }

    public boolean playerExists(FPlayer player) {
        return players.contains(player);
    }

    public FPlayer getPlayer(Player player) {
        for(FPlayer opp : players) {
            if(opp.getPlayer() == player) {
                return opp;
            }
        }
        return null;
    }

    public FPlayer getPlayer(UUID player) {
        for(FPlayer opp : players) {
            if(opp.getUniqueId().toString().equalsIgnoreCase(player.toString())) {
                return opp;
            }
        }
        return null;
    }

    public FPlayer getOfflinePlayer(String name) {
        int ID = sql.getPlayerID(name);
        return FPlayerUtils.buildFPlayerFromSQL(ID);
    }

    public FPlayer getOfflinePlayer(int ID, int a) {
        return FPlayerUtils.buildFPlayerFromSQL(ID);
    }

    public FPlayer getOfflinePlayer(UUID UniqueId) {
        int ID = sql.getPlayerID(UniqueId);
        return FPlayerUtils.buildFPlayerFromSQL(ID);
    }

    public FPlayer getFirst() {
        return players.get(0);
    }

}
