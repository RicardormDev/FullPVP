package net.gitlab.richthelord.FullPVP.Player;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlayerUtils {

    public static final String NoPlayer = "§cJugador Offline.";
    public static final String ErrorCMD = "§cError de Comando. /pvp admin help.";
    public static final String NumberError = "§cError de Número";
    public static final String InternalError = "§cError Interno. Reportar a Dev.";
    public static final String Nuller = "§cNULL.";

    public enum Getters {
        LEVEL, XP, KILLS, ASSISTS, DEADS, KDA
    }

    public static String addTo(Getters getters, String[] args)  {
        if(args.length < 5) return ErrorCMD;

        Player target = Bukkit.getPlayer(args[4]);

        if(target == null) return NoPlayer;
        if(!args[3].matches("[0-9]+")) return NumberError;

        int number = Integer.parseInt(args[3]);

        FPlayer fPlayer = FPlayerManager.getManager().getPlayer(target);
        if(fPlayer == null) return InternalError;

        if(getters == Getters.KILLS) {

            int old = fPlayer.getKills();
            int newData = old + number;

            fPlayer.setKills(newData);

            return "Las kills de " + target.getName() + " fueron actualizadas.";

        } else if(getters == Getters.LEVEL) {

            int old = fPlayer.getLevel();
            int newData = old + number;

            fPlayer.setKills(newData);

            return "El nivel de " + target.getName() + " fueron actualizadas.";

        } else if(getters == Getters.XP) {

            long old = fPlayer.getXP();
            long newData = old + number;

            fPlayer.setXP(newData);

            return "La experiencia de " + target.getName() + " fueron actualizadas.";

        }

        return InternalError;
    }

    public static String removeTo(Getters getters, String[] args)  {
        if(args.length < 5) return ErrorCMD;

        Player target = Bukkit.getPlayer(args[4]);

        if(target == null) return NoPlayer;
        if(!args[3].matches("[0-9]+")) return NumberError;

        int number = Integer.parseInt(args[3]);

        FPlayer fPlayer = FPlayerManager.getManager().getPlayer(target);
        if(fPlayer == null) return InternalError;

        if(getters == Getters.KILLS) {

            int old = fPlayer.getKills();
            int newData = old + number;
            if( newData <= 0) newData = 0;

            fPlayer.setKills(newData);

            return "Las kills de " + target.getName() + " fueron actualizadas.";

        } else if(getters == Getters.LEVEL) {

            int old = fPlayer.getLevel();
            int newData = old + number;
            if( newData <= 0) newData = 0;

            fPlayer.setKills(newData);

            return "El nivel de " + target.getName() + " fué actualizadas.";

        } else if(getters == Getters.XP) {

            long old = fPlayer.getXP();
            long newData = old - number;
            if( newData <= 0) newData = 0;

            fPlayer.setXP(newData);

            return "La experiencia de " + target.getName() + " fueron actualizadas.";

        }

        return InternalError;
    }

    public static String setTo(Getters getters, String[] args)  {
        if(args.length < 5) return ErrorCMD;

        Player target = Bukkit.getPlayer(args[4]);

        if(target == null) return NoPlayer;
        if(!args[3].matches("[0-9]+")) return NumberError;

        int number = Integer.parseInt(args[3]);

        FPlayer fPlayer = FPlayerManager.getManager().getPlayer(target);
        if(fPlayer == null) return InternalError;

        if(getters == Getters.KILLS) {

            if( number <= 0) number = 0;

            fPlayer.setKills(number);

            return "Las kills de " + target.getName() + " fueron actualizadas.";

        } else if(getters == Getters.LEVEL) {

            if( number <= 0) number = 0;

            fPlayer.setLevel(number);

            return "El nivel de " + target.getName() + " fué actualizadas.";

        } else if(getters == Getters.XP) {

            if( number <= 0) number = 0;

            fPlayer.setXP(number);

            return "La experiencia de " + target.getName() + " fueron actualizadas.";

        }

        return InternalError;
    }

    public static String getOf(Getters getters, String[] args)  {
        if(args.length < 4) return ErrorCMD;

        Player target = Bukkit.getPlayer(args[3]);
        if(target == null) return NoPlayer;

        FPlayer fPlayer = FPlayerManager.getManager().getPlayer(target);
        if(fPlayer == null) return Nuller;

        if(getters == Getters.KILLS) {
            int old = fPlayer.getKills();
            return "Las kills de " + target.getName() + " son " + old + ".";
        } else if(getters == Getters.LEVEL) {
            int old = fPlayer.getLevel();
            return "El nivel de " + target.getName() + " es " + old + ".";
        } else if(getters == Getters.XP) {
            long old = fPlayer.getXP();
            return "La XP de " + target.getName() + " es " + old + ".";
        }

        return InternalError;
    }

}

/*
    sender.sendMessage(ChatColor.GOLD + "/pvp admin  add [level | kills | xp ] [number] [player]");
    sender.sendMessage(ChatColor.GOLD + "/pvp admin  set [level | kills | xp ] [number] [player]");
    sender.sendMessage(ChatColor.GOLD + "/pvp admin  remove [level | kills | xp ] [number] [player]");
    sender.sendMessage(ChatColor.GOLD + "/pvp admin  get [level | kills | xp ] [player]");
* */
