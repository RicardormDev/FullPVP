package net.gitlab.richthelord.FullPVP.Player;

import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang.StringUtils;

public enum PVPTitle {
    RECLUTA(ChatColor.AQUA, 0, 0), INICIADO(ChatColor.AQUA, 10, 1), NOVICIO(ChatColor.AQUA, 20, 2), APRENDIZ(ChatColor.DARK_AQUA, 30, 3), SIERVO(ChatColor.DARK_AQUA, 50, 4),
    SOLDADO(ChatColor.GREEN, 80, 5), DISCIPULO(ChatColor.GREEN, 120, 6), MILICIANO(ChatColor.DARK_GREEN, 200, 7), MERCENARIO(ChatColor.DARK_GREEN, 300, 8),
    GUERRERO(ChatColor.RED, 500, 9), ASESINO(ChatColor.RED, 800, 10), VETERANO(ChatColor.RED, 1000, 11), LIDER(ChatColor.RED, 2500, 12), MAESTRO(ChatColor.RED, 5000, 13),
    LEYENDA(ChatColor.GOLD, 10000, 14);

    private ChatColor color;
    private int levelGet;
    private int order;

    PVPTitle(ChatColor c, int levelGet, int order) {
        this.color = c;
        this.levelGet = levelGet;
        this.order = order;
    }

    public ChatColor getColor() {
        return color;
    }

    public int getLevelGet() {
        return levelGet;
    }

    public String getTag() {
        return this.toString().substring(0, 1).toUpperCase() + this.toString().substring(1).toLowerCase();
    }

    public int getOrder() {
        return order;
    }
}
