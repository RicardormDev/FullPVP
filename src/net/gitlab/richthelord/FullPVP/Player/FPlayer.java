package net.gitlab.richthelord.FullPVP.Player;

import be.maximvdw.placeholderapi.PlaceholderAPI;
import net.gitlab.richthelord.FullPVP.Main;
import net.gitlab.richthelord.FullPVP.Utils.TitleUtils;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.UUID;

public class FPlayer {

    private Player player;


    private int kills;
    private int deads;
    private double kda;
    private int assistances;

    private PVPTitle title;
    private int level;
    private long XP;

    private UUID uniqueId;
    private String name;
    private int ID; // SQL ID

    public FPlayer(Player player) {
        this.player = player;
        this.kills = 0;
        this.deads = 0;
        this.kda = (deads == 0) ? 0 : kills / deads;
        this.title = PVPTitle.RECLUTA;
        this.level = 1;
        this.XP = 200;
        this.assistances = 0;

        uniqueId = player.getUniqueId();
        name = player.getName();
    }

    public FPlayer() {
        this.player = null;
        this.kills = 0;
        this.deads = 0;
        this.kda = (deads == 0) ? 0 : kills / deads;
        this.title = PVPTitle.RECLUTA;
        this.level = 1;
        this.XP = 200;
        this.assistances = 0;

        this.ID = 0;
    }

    public Player getPlayer() {
        return player;
    }

    public UUID getUniqueId(){
        return uniqueId;
    }

    public String getName() {
        return name;
    }

    public double getKda() {
        return kda;
    }

    public int getDeads() {
        return deads;
    }

    public int getKills() {
        return kills;
    }

    public int getAssistances(){
        return assistances;
    }

    public int getLevel() {
        return level;
    }

    public PVPTitle getTitle() {
        return title;
    }

    public long getXP() {
        return XP;
    }

    public void setXP(long XP) {
        this.XP = XP;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public void setAssistances(int assistances) {
        this.assistances = assistances;
    }

    public void setDeads(int deads) {
        this.deads = deads;
    }

    public void setKda(double kda) {
        this.kda = kda;
    }

    public void setKills(int kills) {
        this.kills = kills;
    }

    public void setLevel(int level) {
        this.level = level;
        this.XP = 200 * level;
        updateLevel();
    }

    public void setTitle(PVPTitle title) {
        this.title = title;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public void setUniqueId(UUID uniqueId) {
        this.uniqueId = uniqueId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void updateTitle() {
        PVPTitle currentNextTitle = TitleUtils.getNext(title);

        while(level > currentNextTitle.getLevelGet()) {
            title = currentNextTitle;
            PVPTitle checkNext = TitleUtils.getNext(currentNextTitle);
            if(checkNext == title) {
                //puedes devolver null en caso de que no haya un titulo siguiente, en el caso de que se haya usado getNext() en el ultimo titulo disponible.
                break;
            }
            currentNextTitle = checkNext;
        }
    }

    public void updateLevel() {
        level = (int) (XP / 200);
        if((XP % 200) == 0) {
            updateTitle();
        }
    }

    public void updateKill() {
        XP = XP + 40;
        kills += 1;
        int oL = level;
        updateLevel();

        if(oL != level) {
            player.sendMessage(c("&8&l[&3&lVolk&c&lPvP&8&l]&a Has subido al nivel &6" + level + "&a, suerte en el campo de batalla!"));
        }

        updateKDA();
    }

    public void updateAssistance() {
        XP = XP + 20;
        assistances += 1;
        int oL = level;
        updateLevel();
        if(oL != level) {
            player.sendMessage(c("&8&l[&3&lVolk&c&lPvP&8&l]&a Has subido al nivel &6" + level + "&a, suerte en el campo de batalla!"));
        }
        updateKDA();
    }

    public int getID() {
        return ID;
    }

    public void updateDead() {
        deads += 1;
        updateKDA();
    }

    public void updateKDA() {
        this.kda = (deads == 0) ? kills : kills / deads;
    }

    private String c(String m ) {
         return ChatColor.translateAlternateColorCodes('&', m);
    }
}
